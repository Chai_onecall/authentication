<?php

namespace App\Http\Controllers\ManuallyAuth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function login(Request $request)
    {
        return view('authManual.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $user = User::all();

        $foo = Hash::check($credentials['password'], $user->getAuthPassword());

        dd($foo);

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('home');
        }
    }
}
